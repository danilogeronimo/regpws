const express = require("express");
const consign = require('consign');
const bodyParser = require("body-parser");
const cors = require('cors');
const app = express();
const firebase = require('firebase-admin');

const allowedOrigins = [
    'capacitor://localhost',
    'ionic://localhost',
    'http://localhost',
    'http://localhost:8080',
    'http://localhost:8100',
    'http://10.10.10.5:8100',
    'http://10.10.10.2:8100'
]; 

const corsOptions = {
    origin: (origin, callback) => {
        if (allowedOrigins.includes(origin) || !origin) {
            callback(null, true);
        } else {
            callback(new Error('Origin not allowed by CORS'));
        }
    }
}

app.options('*', cors(corsOptions));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors());



const rotas = require("../app/rotas");
rotas(app);


consign( { cwd:  'src' } )
    .include('persistencia')
    .then('schemas')
    .into(app);

module.exports = app;