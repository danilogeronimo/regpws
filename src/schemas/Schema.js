function Schema(conn){
    this._conn = conn;
}

Schema.prototype.montaTabela = function(){
    var sql = "CREATE TABLE IF NOT EXISTS `regp`.`ponto` ( `id` INT NOT NULL AUTO_INCREMENT , `usuario` INT(11) NOT NULL , `sync` BOOLEAN NOT NULL , `data` DATETIME NOT NULL ,`data_ponto` DATETIME NOT NULL, `modificado` BOOLEAN NOT NULL , `motivo` VARCHAR(255), `tipo` INT(2) NOT NULL , `uuid` VARCHAR(255), PRIMARY KEY (`id`));";
    this._conn.query(sql, function (error, results, fields){
        if(error) return console.log(error);
        console.log('Criada a tabela ponto');
    });

    var sql = "CREATE TABLE IF NOT EXISTS `regp`.`usuario` ( `id` INT NOT NULL AUTO_INCREMENT , `nome` VARCHAR(255) NOT NULL , `sync` BOOLEAN NOT NULL , `data` TIMESTAMP NOT NULL ,  `email` VARCHAR(255) NOT NULL, `senha` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`));";
    this._conn.query(sql, function (error, results, fields){
        if(error) return console.log(error);
        console.log('Criada a tabela usuario');
    });

    var sql = "INSERT INTO usuario(nome,sync,email,senha) VALUES('danilo',true,'danilo.geronimo@hotmail.com','123');";
    this._conn.query(sql, function (error, results, fields){
        if(error) return console.log(error);
        console.log('Usuario cadastrado na tabela');
    });
}

module.exports = function(){
    return Schema;
}