const UsuarioControlador = require("./controladores/usuario-controlador");
const usuarioControlador = new UsuarioControlador();

module.exports = (app) => {
    const rotasUsuario = UsuarioControlador.rotas();    
    // app.get(rotasUsuario.lista, usuarioControlador.lista());
    app.post(rotasUsuario.autentica, usuarioControlador.autentica())
    
}