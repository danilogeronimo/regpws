class SincronizaControlador {
    static rotas() {
        return {
            sincroniza: "/sincroniza",
            lista: "/lista",
            schema: "/schema"
        };
    }

    schema(app) {
        return function (req, res) {
            const conexao = app.persistencia.ConexaoDb();
            const schemaDao = new app.schemas.Schema(conexao);
            schemaDao.montaTabela();
        }
    }

    lista(app) {
        return function (req, res) {
            const conexao = app.persistencia.ConexaoDb();
            const pontoDao = new app.persistencia.PontoDao(conexao);

            pontoDao.lista((erro, result) => {
                res.json(result);
            })
        }
    }

    sincroniza(app) {
        let lista;

        return function (req, res) {
            const dadosApp = req.body;
            console.log("Recebendo dados" + JSON.stringify(dadosApp));

            const conexao = app.persistencia.ConexaoDb();
            const pontoDao = new app.persistencia.PontoDao(conexao);

            //realiza uma query simples para verificar a conexão
            pontoDao.verificaConexao((erro, result) => {
                if (erro) {
                    res.status(500).send()
                    console.log(erro)
                }
                else {
                    //se o banco estiver online
                    console.log('BD conectado')
                    lista = result;
                    let dadosAsync;

                    if (dadosApp !== "") {
                        //executa o map e para cada ponto são executados dois métodos(buscaPorId e salva) que retornam promises. 
                        //Ao terminar a execução dos métodos a promise é salva 
                        //na variável dadosAsync para ser executada depois.
                        if (dadosApp.syncAll !== undefined && dadosApp.syncAll !== false) {
                            console.log('sync all') //verificar se precisa implementar
                        } else {
                            dadosAsync = dadosApp.map(dado => {
                                return pontoDao.buscaPorId(dado.uuid)
                                    .then((resultado) => {
                                        //se não encontrar então será salvo no banco
                                        if (!resultado || resultado.length == 0) {
                                            //altera o status dos pontos para sincronizado
                                            dado.sync = 1;
                                            return pontoDao.salva(dado)
                                                .catch(erro => console.log('erro ao salvar: ' + erro));
                                        } else {
                                            //se encontrar atualiza
                                            // return pontoDao.atualiza(dado)
                                            //     .catch(erro => console.log('erro ao atualizar: ' + erro));
                                        }
                                    })
                                    .catch((erro) => {
                                        console.log('erro ao buscar: ' + erro)
                                    });
                            });
                        }
                    }

                    //Assim que os dois métodos acima terminarem de ser executados é chamado o método abaixo que executará as promises e retornará uma lista
                    //para a aplicação com os dados salvos no banco
                    Promise.all(dadosAsync)
                        .then(() => {
                            pontoDao.lista((erro, result) => {
                                if (erro) console.log(erro)
                                else {
                                    console.log('enviando todos os dados: ' + JSON.stringify(result));
                                    res.json(result)
                                };
                            })
                        })
                        .catch(erro => {
                            console.log('Erro ao retornar promises ' + erro);
                            res.status(500).json(erro);
                        })
                };
            });
        }
    }
}

module.exports = SincronizaControlador;