
class UsuarioControlador {
    static rotas() {
        return {
            autentica: "/usuarios/autentica"
        };
    }

    autentica() {
        return function (req, res) {
            console.log('tentativa de login', JSON.stringify(req.body));

            const UsuarioDao = require("../infra/usuario-dao");
            const usuarioDao = new UsuarioDao();
            
            usuarioDao.buscaUsuario('id')
                .then(response => {
                    if (!response) {
                        res.status(500).send({
                            "status": "error",
                            "msg":"Usuário não cadastrado!"
                        });
                        return;
                    }
                    if (response.usuario.dados.email != req.body.dados.email || response.usuario.dados.senha != req.body.dados.senha) {
                        res.status(500).json({
                            "status": "error",
                            "msg":"Usuário ou senha inválidos"
                        });
                        return;
                    }
                    
                    res.status(200).send();

                }).catch(erro => {
                    res.status(500).json({
                        "status": "error",
                        "msg":"Erro ao buscar usuário " + erro
                    });
                    return;
                })
        }
    }
}

module.exports = UsuarioControlador;