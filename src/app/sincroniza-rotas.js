const SincronizaControlador = require("./controladores/sincroniza-controlador");
const sincronizaControlador = new SincronizaControlador();

module.exports = (app) => {
    const rotasSincroniza = SincronizaControlador.rotas();
    app.post(rotasSincroniza.sincroniza, sincronizaControlador.sincroniza(app));
    app.get(rotasSincroniza.schema, sincronizaControlador.schema(app));
    app.get(rotasSincroniza.lista, sincronizaControlador.lista(app));
    
}