const usuarioRotas = require('./usuario-rotas');
const sincronizaRotas = require('./sincroniza-rotas');

module.exports = (app) => {    
    usuarioRotas(app);
    sincronizaRotas(app);
}