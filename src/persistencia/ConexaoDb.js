const mysql = require('mysql');

function conectaBD(){    
    return mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'',
        database:'regp'
    });
}

module.exports = function(){
    return conectaBD;
}

