function PontoDao(conn){
    this._conn = conn; //entender pq perde a referência pelo this ao usar promise
   
}

//função responsável por salvar os pontos no banco. É passado o ponto, a referência para o this e retorna uma promise
PontoDao.prototype.salva = function(ponto, that = this._conn){
    return new Promise(function(resolve,reject){
        console.log(ponto);
        that.query('INSERT INTO ponto SET ?', ponto, function(err,data){
            if(err) reject(err);
            else resolve(data);
        });
    });
}

// PontoDao.prototype.atualiza = function(ponto, that = this._conn) {
//     return new Promise(function(resolve,reject){
//         that.query('UPDATE ponto SET motivo = ?, modificado = ? where uuid = ?', [ponto.motivo,ponto.modificado, ponto.uuid], function(err,data){
//             if(err) reject(err);
//             else resolve(data);
//         });
//     });
// }

//lista registros de pontos salvos no bd
PontoDao.prototype.lista = function(callback) {
    this._conn.query("SELECT id,usuario,sync,data_ponto,modificado,motivo,tipo,uuid from ponto",callback);
}

//executa uma query simples para verificar se o banco está conectado
PontoDao.prototype.verificaConexao = function(callback) {
    this._conn.query("SELECT id,usuario,sync,data_ponto,modificado,motivo,tipo,uuid from ponto LIMIT 1",callback);
}

//função responsável por buscar os pontos pela uuid no banco. É passado a id do ponto, a referência para o this e retorna uma promise
PontoDao.prototype.buscaPorId = function(id,that = this._conn){
    return new Promise(function(resolve,reject){
        that.query("select * from ponto where uuid = ?",[id],function(err,data){
            if(err) reject(err);
            else resolve(data);
        });
    });
}

module.exports = function(){
    return PontoDao;
};